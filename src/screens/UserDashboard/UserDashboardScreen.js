import React from "react";
import { Container } from "native-base";

import UserInfo from "~/components/UserDashboard/UserInfo";

const DashboardScreen = () => {
  return (
    <Container>
      <UserInfo />
    </Container>
  );
};

export default DashboardScreen;
