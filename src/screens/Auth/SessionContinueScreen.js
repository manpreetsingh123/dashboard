import React from "react";
import { StyleSheet } from "react-native";
import { Container } from "native-base";
import ContinueAsUser from "~/components/Auth/ContinueAsUser";

const SessionContinueScreen = () => {
  return (
    <Container style={styles.container}>
      <ContinueAsUser />
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
    padding: 10
  }
});

export default SessionContinueScreen;
