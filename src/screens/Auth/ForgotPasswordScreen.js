import React from "react";
import { StyleSheet } from "react-native";
import { Container } from "native-base";

import ForgotPasswordForm from "~/components/Auth/ForgotPasswordForm";
import LogoWithDescription from "~/components/Shared/LogoWithDescription";

const ForgotPasswordScreen = () => {
  return (
    <Container style={styles.container}>
      <LogoWithDescription description="Please Enter username for password reset" />
      <ForgotPasswordForm />
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
    padding: 10
  }
});

export default ForgotPasswordScreen;
