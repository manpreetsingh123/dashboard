import { combineReducers } from "@reduxjs/toolkit";
import user from "./User.slice";

// Root reduce / combined reducers for the store
const reducer = combineReducers({
  user,
});

export default reducer;
