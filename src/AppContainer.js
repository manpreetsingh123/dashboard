import "react-native-gesture-handler";
import React, { Component } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { Provider } from "react-redux";
import * as Font from "expo-font";

import App from "./App";

import appStore from "./store/";
import { View } from "react-native";

export default class AppContainer extends Component {
  state = {
    loading: true
  };

  async componentDidMount() {
    await Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });
    this.setState({ loading: false });
  }

  render() {
    return (
      <Provider store={appStore}>
        <NavigationContainer>{this.state.loading ? <View /> : <App />}</NavigationContainer>
      </Provider>
    );
  }
}
