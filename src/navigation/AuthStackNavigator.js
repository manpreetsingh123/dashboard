import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import { AuthNavigationScreens } from "~/shared/constants/NavigationScreens";

import SessionContinueScreen from "~/screens/Auth/SessionContinueScreen";
import SigninScreen from "~/screens/Auth/SigninScreen";
import ForgotPasswordScreen from "~/screens/Auth/ForgotPasswordScreen";
import SignupScreen from "~/screens/Auth/SignupScreen";

const AuthStack = createStackNavigator();

const AuthStackNavigator = () => {
  return (
    <AuthStack.Navigator initialRouteName={AuthNavigationScreens.SignIn.name}>
      <AuthStack.Screen
        name={AuthNavigationScreens.ContinueSession.name}
        component={SessionContinueScreen}
        options={{
          headerTitle: AuthNavigationScreens.ContinueSession.title,
          headerShown: false
        }}
      />
      <AuthStack.Screen
        name={AuthNavigationScreens.SignIn.name}
        component={SigninScreen}
        options={{ headerTitle: AuthNavigationScreens.SignIn.title, headerShown: false }}
      />
      <AuthStack.Screen
        name={AuthNavigationScreens.SignUp.name}
        component={SignupScreen}
        options={{ headerTitle: AuthNavigationScreens.SignUp.title }}
      />
      <AuthStack.Screen
        name={AuthNavigationScreens.ForgotPassword.name}
        component={ForgotPasswordScreen}
        options={{ headerTitle: AuthNavigationScreens.ForgotPassword.title }}
      />
    </AuthStack.Navigator>
  );
};

export default AuthStackNavigator;
