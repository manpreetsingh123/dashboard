import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import { MainNavigationScreens } from "~/shared/constants/NavigationScreens";

import UserDashboardScreen from "~/screens/UserDashboard/UserDashboardScreen";

const MainStack = createStackNavigator();

const MainStackNavigator = () => {
  return (
    <MainStack.Navigator>
      <MainStack.Screen
        name={MainNavigationScreens.Dashboard.name}
        component={UserDashboardScreen}
        options={{ headerShown:false,headerTitle: MainNavigationScreens.Dashboard.title }}
      />
    </MainStack.Navigator>
  );
};

export default MainStackNavigator;
