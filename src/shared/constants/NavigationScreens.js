export const AuthNavigationScreens = {
  SignIn: {
    name: "AuthSigninScreen",
    title: "Sign In"
  },
  ContinueSession: {
    name: "ContinueSession",
    title: "Continue Session"
  },
  SignUp: {
    name: "AuthSignupScreen",
    title: "Sign Up"
  },
  ForgotPassword: {
    name: "AuthForgotPasswordScreen",
    title: "Forgot Password"
  }
};

export const MainNavigationScreens = {
  Dashboard: {
    name: "MainDashboardScreen",
    title: "Dashboard"
  },
  UnlockExam: {
    name: "UnlockExamScreen",
    title: "Unlock Exam"
  }
};
