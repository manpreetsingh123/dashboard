import React from "react";
import { StyleSheet, View, TouchableOpacity, Text } from "react-native";
import { useNavigation } from "@react-navigation/native";

import { useDispatch } from "react-redux";
import { signin } from "~/store/slices/User.slice";
import { AuthNavigationScreens } from "~/shared/constants/NavigationScreens";

const ContinueAsUser = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const continueSession = (data) => {
    dispatch(signin());
  };

  const startNewSession = () => {
    // clear current user session

    navigation.navigate(AuthNavigationScreens.SignIn.name);
  };

  return (
    <View style={styles.actionsContainer}>
      <Text>Random Person</Text>
      <Text>University Name</Text>
      <TouchableOpacity onPress={continueSession}>
        <Text>Continue as Random Person</Text>
      </TouchableOpacity>

      <TouchableOpacity onPress={startNewSession}>
        <Text>No thanks. Sign in as a different user.</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff"
  }
});

export default React.memo(ContinueAsUser);
