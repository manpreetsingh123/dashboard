import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import { Form } from "native-base";
import { useForm } from "react-hook-form";
import { useNavigation } from "@react-navigation/native";

import UITextInput from "../UI/Inputs/UITextInput";
import UIButton from "../UI/Inputs/UIButton";
import UICheckBox from "../UI/Inputs/UICheckBox";
import UILinkButton from "../UI/Inputs/UILinkButton";

import { useDispatch } from "react-redux";
import { signin } from "~/store/slices/User.slice";

const SigninForm = () => {
  const {
    control,
    handleSubmit,
    formState: { errors }
  } = useForm();

  const dispatch = useDispatch();
  const navigation = useNavigation();

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const forgotPasswordHandler = () => {
    navigation.navigate("AuthForgotPasswordScreen");
  };

  // const signUpHandler = () => {
  //   navigation.navigate("AuthSignupScreen");
  // };

  const onSubmit = (data) => {
    if (username !== "" && password !== "") {
      let userData = {
        userId: username,
        fname: username,
        lname: username
      };
      dispatch(signin(userData));
    }
  };

  return (
    <Form style={styles.form}>
      <UITextInput
        label="Username"
        style={styles.formInput}
        value={username}
        onChange={setUsername}
      />
      <UITextInput
        label="Password"
        style={styles.formInput}
        value={password}
        onChange={setPassword}
        isPassword
      />
      <View style={styles.actionsContainer}>
        <UICheckBox label="Remember me" checked={false} />
        <UILinkButton onClick={forgotPasswordHandler}>Forgot Password?</UILinkButton>
      </View>
      <View style={styles.actionsContainer}>
        <UIButton style={{ marginRight: 5 }} onClick={onSubmit} full>
          Login
        </UIButton>
        {/* <UIButton style={{ marginLeft: 5 }} full bordered onClick={signUpHandler}>
          Sign up
        </UIButton> */}
      </View>
    </Form>
  );
};

const styles = StyleSheet.create({
  form: {
    width: "100%",
    maxWidth: 350,
    marginTop: 35
  },
  formInput: {
    marginBottom: 15
  },
  actionsContainer: {
    width: "100%",
    marginBottom: 15,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between"
  }
});

export default React.memo(SigninForm);
