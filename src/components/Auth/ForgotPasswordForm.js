import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import { Form, Text } from "native-base";
import { useForm, Controller } from "react-hook-form";
import { useNavigation } from "@react-navigation/native";

import UITextInput from "../UI/Inputs/UITextInput";
import UIButton from "../UI/Inputs/UIButton";
import reducer from "~/store/slices";

const ForgotpasswordForm = () => {
  const {
    control,
    handleSubmit,
    formState: { errors }
  } = useForm();

  const onSubmit = (data) => {
    console.log(data);
  };

  const navigation = useNavigation();

  const loginHandler = () => {
    navigation.navigate("AuthSigninScreen");
  };

  return (
    <Form style={styles.form}>
      <Controller
        name="Username"
        control={control}
        rules={{ required: "Username required" }}
        render={({ field: { onChange, value } }) => (
          <UITextInput
            value={value}
            label="Username"
            onChange={(val) => onChange(val)}
            style={styles.formInput}
          />
        )}
      />
      {errors.Username && <Text style={styles.errprMsg}>{errors.Username?.message}</Text>}

      <View style={styles.actionsContainer}>
        <UIButton style={{ marginRight: 5 }} onClick={handleSubmit(onSubmit)}>
          Forgot Password
        </UIButton>
        <UIButton style={{ marginLeft: 5 }} onClick={loginHandler}>
          Login
        </UIButton>
      </View>
    </Form>
  );
};

const styles = StyleSheet.create({
  form: {
    width: "100%",
    maxWidth: 350,
    marginTop: 35
  },
  formInput: {
    marginBottom: 10
  },
  actionsContainer: {
    marginBottom: 10,
    display: "flex",
    flexDirection: "row"
  },
  errprMsg: {
    marginBottom: 10,
    color: "#ff0000"
  }
});

export default React.memo(ForgotpasswordForm);
