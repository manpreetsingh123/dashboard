import { View,Text,StyleSheet } from 'react-native';
import React from 'react';
import { Container, Left, Content, Right, Icon, List,ListItem } from 'native-base';

const ScheduleTable=()=>{
    return (
      <View style={styles.secStyle}>
          <Container>
            <Content>
            <List>
              <ListItem noIndent style={{ backgroundColor: "#e8e9ec" }}>
                <Left>
                  <Text>SUBJECT</Text>
                </Left>
                <Left>
                  <Text>SEMESTER</Text>
                </Left>
                <Left>
                  <Text>DAY</Text>
                </Left>
                <Left>
                  <Text>DATE</Text>
                </Left>
                <Right>
                  <Text>ACTION</Text>
                </Right>
              </ListItem>
              <ListItem >
                <Left>
                  <Text>SEM III</Text>
                </Left>
                <Left>
                  <Text>PHYSICS 1</Text>
                </Left>
                <Left>
                  <Text>TUESDAY</Text>
                </Left>
                <Left>
                  <Text>13/05/2021</Text>
                </Left>
                <Right>
                  <Icon name='ellipsis-horizontal-outline' style={styles.Icons} />
                </Right>
              </ListItem>
              <ListItem >
                <Left>
                  <Text>SEM III</Text>
                </Left>
                <Left>
                  <Text>PHYSICS 1</Text>
                </Left>
                <Left>
                  <Text>WEDNESDAY</Text>
                </Left>
                <Left>
                  <Text>14/05/2021</Text>
                </Left>
                <Right>
                  <Icon name='ellipsis-horizontal-outline' style={styles.Icons} />
                </Right>
              </ListItem>
              <ListItem >
                <Left>
                  <Text>SEM IV</Text>
                </Left>
                <Left>
                  <Text>PHYSICS 1</Text>
                </Left>
                <Left>
                  <Text>FRIDAY</Text>
                </Left>
                <Left>
                  <Text>16/05/2021</Text>
                </Left>
                <Right>
                  <Icon name='ellipsis-horizontal-outline' style={styles.Icons} />
                </Right>
              </ListItem>
            </List>
            </Content>
          </Container>
        </View>
    );
  }

const Opt=(props)=>{
  return(
    <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:18,marginBottom:15}}>
             <Text style={styles.allOpt}>{props.title}</Text>
          
             <Text style={styles.allOpt}>VIEW ALL</Text>
    </View>
  )
}
  export {Opt}
  export default ScheduleTable;

  const styles = StyleSheet.create({
      secStyle:{
      shadowColor: "lightgrey",
      shadowOffset: {
      width: 0,
      height: 4,
      },
      shadowOpacity: 1,
      shadowRadius: 5.46,
      elevation: 9,

      },
      allOpt:{
        fontWeight:'bold'
      }
   });
   