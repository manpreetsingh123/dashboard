import React from 'react'
import {Text} from 'react-native'
import { Body, Left, Right, Icon, ListItem } from 'native-base';

const ListItemData=(props)=>{
    return(
      <ListItem avatar>
        <Left>
          <Text style={{
          paddingHorizontal:15,
          paddingVertical:12,
          height:43,
          color:'#fff',
          textAlign:'center',
          backgroundColor:'#68a0cf',
          borderRadius: 100}}>
            {props.classLogo}
          </Text>
        </Left>
        <Body style={{marginLeft:52}}>
          <Text>{props.className}</Text>
          <Text style={{fontSize:13,color:'#8e7e7e'}} note>{props.classSyllabus}</Text>
        </Body>
        <Right style={{paddingTop:18}}>
          <Icon name='ellipsis-horizontal-outline' style={{}} />
        </Right>
      </ListItem>
    )
}

export default ListItemData;