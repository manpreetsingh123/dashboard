import React from 'react'
import { StyleSheet } from "react-native";
import { Button, Icon} from 'native-base';

const LeftBarIcon=(props)=>{
    return(
   <>
    <Button style={[styles.leftItems,props.activeTab]} transparent>
      <Icon name={props.IcnName} style={styles.Icons} />
    </Button>
   </>
    )
}

export default LeftBarIcon;
const styles = StyleSheet.create({
    leftItems:{
     paddingBottom:55,
    },
    Icons:{
     color:'#000',
     marginTop:50,
     fontSize:35
    }
   });
   