import React from 'react'
import {SafeAreaView,View,Text,StyleSheet,Image} from 'react-native'
import { Header,Right,Button,Icon} from 'native-base';

const HeaderSec=()=>{
    return(
        <SafeAreaView style={styles.secStyle}>
            <View style={{flexDirection:'row'}}>
               <Text style={{padding:12,fontSize:20,fontWeight:'bold'}}>Dashboard</Text>
            </View>
            <Header style={{backgroundColor:'transparent'}}>
                <Right>
                    <Button transparent>
                       <Icon name='notifications' style={{color:'#lightgrey'}} />
                    </Button>
                    <Button transparent>
                       <Icon name='apps' style={{color:'#lightgrey'}} />
                    </Button>
                    <Button transparent>
                       <Icon name='person' style={{color:'#lightgrey'}} />
                    </Button>
                </Right>
            </Header>
        </SafeAreaView>

    )
}
export default HeaderSec;


const styles = StyleSheet.create({
    secStyle:{
        shadowColor: "lightgrey",
        shadowOffset: {
        width: 0,
        height: 2,
        },
        shadowOpacity: 1,
        shadowRadius: 4,
        flexDirection:'row',
        justifyContent:'space-between',
    }
   });
   