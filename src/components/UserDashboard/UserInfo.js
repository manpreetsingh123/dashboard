import React from "react";
import { Text, View,StyleSheet,SafeAreaView,Image } from "react-native";
import { Content, Button, List } from 'native-base';
import DashTabs from './DashboardComponents//DashboardTabs'
import HeaderSec from './DashboardComponents//Header'
import ScheduleTable, { Opt } from './DashboardComponents/ScheduleTable'
import Calendar from "./DashboardComponents/Calendar";
import ListItemData from "./DashboardComponents/ListItems";
import LeftBarIcon from "./DashboardComponents/LeftMenu";
const UserInfo = () => {
 return (
    <View>
     <View style={[styles.container, {flexDirection: "row"}]}>
      <View style={styles.leftSec}>
          <Button style={styles.logo} transparent>
            <Image style={{width:45,height:45}} source={require('../../assets/images/littlemore-logo-small.jpg')}/>
          </Button>
          <View style={{marginTop:10}}>
            <LeftBarIcon activeTab={styles.activeTab} IcnName={'home'}/>
            <LeftBarIcon IcnName={'book-outline'}/>
            <LeftBarIcon IcnName={'ribbon-outline'}/>
            <LeftBarIcon IcnName={'book-outline'}/>
            <LeftBarIcon IcnName={'calendar-outline'}/>
            <LeftBarIcon activeTab={{borderBottomWidth:1,borderColor:'lightgrey'}} IcnName={'tv-outline'}/>
            <LeftBarIcon IcnName={'grid-outline'}/>
          </View>
      </View>
      <View style={{ flex: 2,marginHorizontal:12}}>
        <HeaderSec />
        <DashTabs /> 
        <SafeAreaView>
        <View style={[styles.container, {flexDirection: "row"}]}>
        <View style={{ flex: 2,borderRightWidth:1,paddingRight:12,borderColor:'lightgrey'}}>
            <SafeAreaView style={{paddingBottom:15}}>
                <Opt title={'EXAM SCHEDULE'}/>
                <ScheduleTable />
            </SafeAreaView>
            <SafeAreaView style={{paddingBottom:15}}>
                <Opt title={'EXAM PERFORMANCE'}/>
                <ScheduleTable />
            </SafeAreaView>
        </View>
        <View style={{ flexDirection:'column',flex:1,paddingTop:10}}>
          <Calendar />
          <Content>
          <View style={{marginHorizontal:20,paddingVertical:5}}>
            <Text style={{fontWeight:'bold'}}>TODAYS SCHEDULE</Text>
          </View>
            <List>
              <ListItemData classLogo="M" className="Math Class" classSyllabus="Chapter 3"/>
              <ListItemData classLogo="P" className="Physics Class" classSyllabus="Chapter 2"/>
              <ListItemData classLogo="S" className="Science Class" classSyllabus="Chapter 1"/>
            </List>
          </Content>
        </View>
        </View>
        </SafeAreaView>
        </View>
    </View>
   </View>
 );
};
 
export default UserInfo;
 
const styles = StyleSheet.create({
 container: {
   flex: 1,
 },
 logo:{
  padding:10,
  paddingTop:20
 },
 leftSec:{
   flex:0.11,
  shadowColor: "lightgrey",
  shadowOffset: {
    width: 0,
    height: 4,
  },
  shadowOpacity: 1,
  shadowRadius: 5.46,
 },
 activeTab:{
  paddingBottom:60,
  backgroundColor:'lightgrey'
 }
});
