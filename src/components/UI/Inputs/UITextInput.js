import React from "react";
import { Input, Item, Label } from "native-base";
import { StyleSheet, View } from "react-native";

const UITextInput = ({ value, label, onChange, style, isPassword, placeholder }) => {
  const onChangeHandler = (e) => {
    if (e) e.preventDefault();
    const value = e.target.value;
    onChange(value);
  };

  return (
    <View style={style}>
      <Label>{label}</Label>
      <Item regular={true} style={styles.root} bordered>
        <Input secureTextEntry={isPassword} onChange={onChangeHandler} defaultValue={value} />
      </Item>
    </View>
  );
};

const styles = StyleSheet.create({
  root: { marginTop: 5, borderRadius: 5 }
});

export default UITextInput;
