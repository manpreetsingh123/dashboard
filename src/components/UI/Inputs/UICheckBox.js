import React, { useState } from "react";
import { CheckBox, Text } from "native-base";
import { StyleSheet, TouchableOpacity } from "react-native";

const UICheckBox = ({ label, value, onChange }) => {
  const [checked, setChecked] = useState(value);
  const toggleHandler = () => {
    setChecked(!checked);
    if (onChange) onChange(!checked);
  };
  return (
    <TouchableOpacity style={styles.container} onPress={toggleHandler}>
      <CheckBox style={styles.checkbox} checked={checked} onPress={toggleHandler} />
      <Text style={styles.label}>{label}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center"
  },
  checkbox: {
    margin: 0
  },
  label: {
    marginLeft: 20
  }
});

export default UICheckBox;
