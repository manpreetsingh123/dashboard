import React from "react";
import { StyleSheet, Platform, View } from "react-native";
import { useSelector } from "react-redux";

import AuthStackNavigator from "./navigation/AuthStackNavigator";
import MainStackNavigator from "./navigation/MainStackNavigator";

export default function App() {
  const isLoggedIn = useSelector((state) => state.user.loggedIn);

  console.log(Platform.OS);

  if (Platform.OS == "web") {
    if (navigator.userAgent.toLowerCase().indexOf("electron/") > -1) {
      console.log(window.process);
      const { ipcRenderer } = window.require("electron");

      // Synchronous message emmiter and handler
      // ipcRenderer.sendSync('synchronous-message', 'sync ping')

      //Async message sender
      ipcRenderer.send("asynchronous-message", "async ping");
    }
  }

  return (
    <View style={styles.container}>
      {isLoggedIn ? <MainStackNavigator /> : <AuthStackNavigator />}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff"
  }
});
